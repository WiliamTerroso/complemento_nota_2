#!/bin/bash

# Verifica se foram fornecidos 4 argumentos de linha de comando
if [ "$#" -ne 4 ]; then
    echo "Por favor, forneça 4 nomes de arquivos como argumentos de linha de comando."
    exit 1
fi

# Função para contar o número de linhas em um arquivo
count_lines() {
    local arquivo="$1"
    wc -l < "$arquivo"
}

maior_num_linhas=0
maior_arquivo=""

# Percorre os argumentos de linha de comando e encontra o arquivo com o maior número de linhas
for arquivo in "$@"; do
    num_linhas=$(count_lines "$arquivo")
    
    if [ "$num_linhas" -gt "$maior_num_linhas" ]; then
        maior_num_linhas="$num_linhas"
        maior_arquivo="$arquivo"
    fi
done

# Exibe o conteúdo do arquivo com o maior número de linhas
echo "Conteúdo do arquivo $maior_arquivo:"
cat "$maior_arquivo"
